
class Data:

    '''

    реализовать геттеры

    '''

    bearer = 'ZGE5OWU0MjktOWY1ZS00NzVjLWI1ZjctYjJiMzI1NDVkMmQy'
    base_url = 'https://api.m3o.com/v1'
    headers = {
        "Content-Type": "application/json",
        "Authorization": f"Bearer {bearer}"
    }

    valid_user_id = "user-macOs-3"
    invalid_user_id = "user-macOs-4"
