from requests import Response
from requests import JSONDecodeError


class BaseHandler:
    """
    Базовый класс, содержащий общие методы для различных тестов

    Args:
        -
    Attributes:
        -
    """
    def handle_cookie(self, response: Response, cookie_name: str, expected_result: str) -> None:
        """
        Метод, проверяющий наличие искомых куки в ответе на запрос и сравнивающий их значение с ожидаемым значением

        :param response: объект ответа на запрос
        :type response: Response
        :param cookie_name: наименование куки
        :type cookie_name: str
        :param expected_result: ожидаемое значение куки
        :type expected_result: str
        """
        assert cookie_name in response.cookies, f"There is no cookie named {cookie_name} in this response"
        cookie_value = response.cookies[cookie_name]
        assert expected_result == cookie_value, f"Expected result not equal current result"

    def handle_header(self, response: Response, header_name: str, expected_result) -> None:
        """
        Метод, проверяющий наличие искомого заголовка в ответе на запрос и сравнивающий его значение с ожидаемым

        :param response: объект ответа на запрос
        :type response: Response
        :param header_name: наименование заголовка
        :type header_name: str
        :param expected_result: ожидаемое значение заголовка
        :type expected_result: str
        """
        assert header_name in response.headers, f"There is no cookie named {header_name} in this response"
        header_value = response.headers[header_name]
        assert expected_result == header_value, f"Expected result not equal current result"

    def cast_payload_to_json(self, response: Response, field_names: list[str]) -> None:
        """
        Метод, проверяющий возможность сериализации JSON в словарь и вызывающий

        :param response: объект ответа на запрос
        :type response: Response
        :param field_names: список
        """
        try:
            data = response.json()
        except JSONDecodeError:
            assert False, f"Can't cast payload to dict from JSON\n{response.content}"
        BaseHandler.compare_objects(field_names, data)

    @staticmethod
    def compare_objects(expected_headers: list[str], response: dict):
        """
        Статический метод, сравнивающий наличие ожидаемых заголовков в списке заголовков из ответа на запрос

        :param expected_headers: список ожидаемых заголовков
        :type expected_headers: list[str]
        :param response: объект ответа на запрос
        :type response: dict
        """

        for field in expected_headers:
            if field in response['account'].keys():
                continue
            else:
                raise Exception(f"There is no such a field '{field}'")
