from requests import Response
from data.data import Data


class Assertions:
    @staticmethod
    def check_status_code(response: Response):
        return Assertions._compare_status_code(response)

    @staticmethod
    def _compare_status_code(response: Response):

        if response.status_code == 200:
            return f"Status code is {response.status_code}, successful answer received"
        elif response.status_code == 401:
            return f"Status code is {response.status_code}, successful answer received"
        elif response.status_code == 400:
            return f"Status code is {response.status_code}, successful answer received"
        elif response.status_code == 500:
            return f"Status code is {response.status_code}, successful answer received"
        else:
            return f"Status code {response.status_code} is unknown"

    @staticmethod
    def compare_user_id(response: Response, expected_id: str):
        id_from_response = response.json()["session"]["userId"]
        if expected_id == Data.valid_user_id:
            assert id_from_response == expected_id, f"There is no client with id {id_from_response} in database"
        else:
            assert id_from_response != Data.invalid_user_id, f"There is client with id {id_from_response} in database, " \
                                                             f"that can't be"
