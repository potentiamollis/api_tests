FROM ubuntu
RUN mkdir app
WORKDIR /app
COPY . .
RUN apt update && apt-get install -y pip
RUN pip install -r requirements.txt
RUN apt-get install -y allure
ENTRYPOINT ["python3"]
CMD ["-m", "pytest", "-s", "tests/"]