import pytest
import requests
import json
import allure
from data.data import Data
from helpers.base_handler import BaseHandler


@allure.epic("User deleting tests")
class TestDeleteUsers(BaseHandler):
    """
    Класс, проверяющие удаление пользователей
    Родительский класс: BaseHandler
    """
    list_of_user_id = [
        {"id": "user-macOs-7"},
        {"id": "user-macOs-8"},
        {"id": "user-macOs-9"},
        {"id": "user-macOs-3"}
    ]

    def setup(self):
        """
        Метод для подготовки данных к тестам

        """
        self.url = f"{Data.base_url}/user/Delete"
        self.data = {"id": "user-macOs-7"}

    # @allure.description("This test checks successfully deleting the user")
    @pytest.mark.parametrize("data", list_of_user_id)
    def test_delete_users(self, data: dict):
        """
        Метод, проверяющий успешное удаление пользователей

        :param data: тестовый заголовок запроса
        :type data: dict
        """
        with allure.step(f"POST request to URL {self.url}"):
            response = requests.post(self.url, data=json.dumps(data), headers=Data.headers)
        assert response.status_code == 200, f"Status code {response.status_code} is not equal 200"
        with allure.step(f"Compare object of json with empty dict"):
            assert response.json() == {}, "Not all users deleted"
