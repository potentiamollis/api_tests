import json
import requests
import allure
from helpers.base_handler import BaseHandler
from data.data import Data

@allure.epic("User creating tests")
class TestCreatingUser(BaseHandler):
    """
    Класс, реализующий тесты по созданию пользователей
    Родительский класс: BaseHandler

    Arguments:
        -
    Attributes:
        -
    """

    def setup(self):
        """
        Метод, подготавливающий данные для выполнения тестов

        :return:
        """
        self.expected_headers = ["email", "id", "username"]
        self.url = f"{Data.base_url}/user/Create"
        self.data = {
            "email": "sample@example.com",
            "id": Data.valid_user_id,
            "password": "P@ssw0rd1234",
            "username": "inno"
        }

    @allure.description("This test checks successfully creating new user")
    def test_create_new_user(self):
        """
        Метод, проверяющий успешность создания нового пользователя

        """
        with allure.step(f"POST request to URL {self.url}"):
            response = requests.post(self.url, data=json.dumps(self.data), headers=Data.headers)
        assert response.status_code == 200, f"Status code {response.status_code} is not equal 200"
        self.cast_payload_to_json(response, self.expected_headers)

    @allure.description("This test checks unsuccessfully creating new user with same credentials")
    def test_create_the_user_again(self):
        """
        Метод, проверяющий не успешность создания нового пользователя с реквизитами существуюшего

        """
        with allure.step(f"POST request to URL {self.url}"):
            response = requests.post(self.url, data=json.dumps(self.data), headers=Data.headers)
        assert response.status_code == 400, f"Status code {response.status_code} is not equal 400"
        assert response.json()["detail"] == "account already exists"
