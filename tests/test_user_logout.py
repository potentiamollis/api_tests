from helpers.base_handler import BaseHandler
from data.data import Data
import requests
import json
import allure


@allure.epic("User logout tests")
class TestUserLogout(BaseHandler):
    """
    Класс, реализующий тесты по разлогированию пользователей
    Родительский класс: BaseHandler
    """

    def setup(self):
        self.url = f'{Data.base_url}/user/Logout'
        with open("data/session_id.txt", "r") as f:
            self.data = f.readline()

    @allure.description("Test checks is successfully user logout")
    def test_success_logout(self):

        response = requests.post(self.url, data=json.dumps({"session_id": self.data}), headers=Data.headers)
        # проверка кода ответа после запроса на разлогин
        assert response.status_code == 200, f"Status code {response.status_code} is not equal 200"

    @allure.description("Test checks is successfully user logout thar already did it")
    def test_repeated_logout(self):
        response = requests.post(self.url, data=json.dumps({"session_id": self.data}), headers=Data.headers)
        assert response.status_code == 500, f"Status code {response.status_code} is not equal 500. Repeated logout can't" \
                                            f"be successful"
