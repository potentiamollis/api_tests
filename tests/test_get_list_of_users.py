import pytest
import requests
import json
import allure
from data.data import Data
from helpers.base_handler import BaseHandler


@allure.epic("Getting list of users tests")
class TestGetUsers(BaseHandler):
    """
    Класс, реализующий тесты по получению списка пользователей
    Родительский класс: BaseHandler

    """

    def setup(self):

        self.expected_headers = ["email", "id", "username"]
        self.url_creating = f"{Data.base_url}/user/Create"
        self.url_listing = f"{Data.base_url}/user/List"
        self.list_of_user_id = ["user-macOs-7", "user-macOs-8", "user-macOs-9"]
        self.actual_list_of_user_id = []

    data = [
        {
            "email": "simple@example.com",
            "id": "user-macOs-7",
            "password": "P3@ssw0rd1234",
            "username": "crown"
        },

        {
            "email": "dimple@example.com",
            "id": "user-macOs-8",
            "password": "P@sswd0rd1234",
            "username": "hot_apple"
        },

        {
            "email": "smple@example.com",
            "id": "user-macOs-9",
            "password": "P@ssqww0rd1234",
            "username": "wildWind"
        }

    ]

    data_list = {
            "limit": 2,
            "offset": 0
        }

    @allure.description("This test checks successfully creating multiple users")
    @pytest.mark.parametrize("data", data)
    def test_create_multiple_users(self, data):
        response = requests.post(self.url_creating, data=json.dumps(data), headers=Data.headers)
        assert response.status_code == 200, f"Status code {response.status_code} is not equal 200"
        with allure.step(f"Check can response be cast to dict from JSON"):
            self.cast_payload_to_json(response, self.expected_headers)

    @allure.description("This test checks successfully getting list of users")
    def test_get_list_of_users(self):
        response = requests.post(self.url_listing, data=json.dumps(self.data_list), headers=Data.headers)
        assert response.status_code == 200, f"Status code {response.status_code} is not equal 200"
        response_body = response.json()
        # перебираем списки с id ожидаемых пользователей с id из списка полученных пользователей
        with allure.step(f"Check are all the created users in current list of active users"):
            for index, username in enumerate(self.list_of_user_id):
                for _ in response_body["users"]:
                    if username in _["id"]:
                        self.actual_list_of_user_id.append(username)
                        break
            assert sorted(self.list_of_user_id) == (sorted(self.actual_list_of_user_id)), f"Not all users id presented in" \
                                                                                      f"response"
