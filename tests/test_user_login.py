import pytest
from helpers.base_handler import BaseHandler
from data.data import Data
from helpers.assertions import Assertions
import requests
import json
import allure


@allure.epic("User logining tests")
class TestUserLogin(BaseHandler):
    """
    Класс, реализующий тесты на логин созданных пользователей
    Родительский класс: BaseHandler
    """

    user_id = [
        Data.valid_user_id,
        Data.invalid_user_id
    ]

    def setup(self):
        self.data = {
            "email": "sample@example.com",
            "password": "P@ssw0rd1234"
        }
        self.url = f'{Data.base_url}/user/Login'
        self.session_id = ''

    @allure.description("This test checks result of login with existing and not existing user")
    @pytest.mark.parametrize("user_id", user_id)
    def test_success_login(self, user_id: str):
        """
        Класс, реализующий тесты на логин клиента, верифированного в системе, или нет

        :param user_id: id пользователя, о котором система может знать или не знать
        :type user_id: str
        """
        response = requests.post(self.url, data=json.dumps(self.data), headers=Data.headers)
        assert response.status_code == 200, f"Status code {response.status_code} is not equal 200"
        # тест реакции системы на попытку логина со стороны верифицированного или не верифицированного клиента
        with allure.step("Test the result of login attempt"):
            Assertions.compare_user_id(response, user_id)
        # Если id пользователя существует, то id сессии после логина сохраняем в файл для следующих тестов
        if user_id == Data.valid_user_id:
            with open("data/session_id.txt", "w") as f:
                f.write(f'{response.json()["session"]["id"]}')
