# api_tests

## Автотесты реализованы для некоторых методов из публичного API https://m3o.com/user


## Установка необходимых сторонних библиотек

### ```pip install -r requirements.txt```
### ``` sudo apt-get install allure``` установка пакета Allure для Linux
###  ``` brew install allure``` установка пакета Allure для MacOS


## Запуск тестов

### Команды для запуска тестов без формирования отчета в Allure

```python3 -m pytest -s tests/ ``` для ОС на UNIX

```python -m pytest -s tests/ ``` для Windows

### Команды для запуска тестов c формированием отчета в Allure

```python3 -m pytest --alluredir=allure_reports/ tests/ ``` формирование отчета по итогам тестирования

```allure serve allure_reports ``` просмотр отчета в web-интерфейсе


## Структура проекта 

- allure-reports - папка для сгенерированных отчетов Allure
- data - вспомогательные данные (логины/пароли, разные id) для запуска тестов
- helpers - содержит классы, в которых реализованы общие методы для использования в разных тестах
- tests - тестовые сценарии
- logs - данные логов исполнения тестов 
